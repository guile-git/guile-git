;;; Guile-Git --- GNU Guile bindings of libgit2
;;; Copyright © 2025 Nicolas Graves <ngraves@ngraves.fr>
;;;
;;; This file is part of Guile-Git.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests notes)
  #:use-module (git)
  #:use-module (git structs)
  #:use-module (tests helpers)
  #:use-module (srfi srfi-64)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)
  #:use-module (ice-9 match))

(test-begin "notes")

(with-repository "simple" directory

  (let* ((repository (repository-open directory))
         (head       (repository-head repository))
         (oid (reference-target head))
         (walker     (revwalk-new repository))
         (author     (signature-new "Author" "author@gnu.org" 0 0))
         (committer  (signature-new "Committer" "committer@gnu.org" 0 0))
         (content    "This is a test note")
         (notes-ref  "ref/notes/commits"))

    (test-assert "note-create and note-read"
      (let ((new-oid (note-create repository
                                  author
                                  committer
                                  oid
                                  content)))
        (test-assert "Note creation returned a valid OID" (oid? new-oid))
        (let ((note (note-read repository oid)))
          (test-assert "Note is successfully read" note)
          (test-equal "Note message matches" content (note-message note))
          ;; Signatures (thus author and committer) are not comparable.
          )))))

(test-end "notes")
