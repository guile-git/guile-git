;;; Guile-Git --- GNU Guile bindings of libgit2
;;; Copyright © 2025 Nicolas Graves <ngraves@ngraves.fr>
;;;
;;; This file is part of Guile-Git.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests revwalker)
  #:use-module (git)
  #:use-module (git structs)
  #:use-module (tests helpers)
  #:use-module (srfi srfi-64)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)
  #:use-module (ice-9 match))

(test-begin "revwalker")

(with-repository "simple" directory

  (let* ((repository (repository-open directory))
         (head       (repository-head repository))
         (master-oid (reference-target head))
         (walker     (revwalk-new repository)))

    (test-assert "revwalk-push!, revwalk-next!, revwalk-reset!"
      (begin
        (revwalk-push! walker master-oid)
        (oid? (revwalk-next! walker))))

    (test-assert "revwalk-reset!"
      (begin
        (revwalk-reset! walker)
        (not (revwalk-next! walker))))

    (test-assert "revwalk-push-range!"
      (begin
        (revwalk-reset! walker)
        (revwalk-push-range! walker "HEAD~2..HEAD")
        (let loop ((oids '()))
          (match (revwalk-next! walker)
            (#f (= (length oids) 2))  ; Ensure we collected commits
            ((? oid? oid) (loop (cons oid oids)))))))

    (test-assert "revwalk-hide!"
      (begin
        (revwalk-reset! walker)
        (revwalk-push! walker master-oid)
        (let* ((initial-commits (let loop ((oids '()))
                                  (match (revwalk-next! walker)
                                    (#f (reverse oids))
                                    ((? oid? oid) (loop (cons oid oids))))))
               (first-oid (car initial-commits)))
          ;; Reset the walker, hide the first commit, and check if it's excluded
          (revwalk-reset! walker)
          (revwalk-hide! walker first-oid)
          (revwalk-push! walker master-oid)
          (not (member first-oid
                       (let loop ((oids '()))
                         (match (revwalk-next! walker)
                           (#f (reverse oids))
                           ((? oid? oid) (loop (cons oid oids))))))))))))

(test-end "revwalker")
